  module.exports = {
      outputDir: 'drag',
      assetsDir: 'static',
      publicPath: './',

    //   pluginOptions: {
    //       'style-resources-loader': {
    //           preProcessor: 'less',
    //           patterns: [
    //               path.resolve(__dirname, './src/assets/css/common/common.less')
    //           ]
    //       }
    //   },
      devServer:{
        port:8080,
        host:'localhost',
        https:false,
        open:true,
        proxy:null,
      },
      chainWebpack: (config) => {
          /* 添加分析工具*/
          if (process.env.NODE_ENV === 'production') {
              if (process.env.npm_config_report) {
                  config
                      .plugin('webpack-bundle-analyzer')
                      .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
                      .end();
                  config.plugins.delete('prefetch')
              }
          }
      },
      configureWebpack: (config) => {
          if (process.env.NODE_ENV === 'production') {
              // 为生产环境修改配置...
              config.mode = 'production'
              return {
                  plugins: [new CompressionPlugin({
                      test: /\.js$|\.html$|\.css|\.png$|\.jpg$|\.gif$|/, //匹配文件名
                      threshold: 1024, //对超过1k的数据进行压缩
                      deleteOriginalAssets: false //是否删除原文件
                  })]
              }
          }
      },
  }