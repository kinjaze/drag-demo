import Vue from 'vue'
import Vuex from 'vuex'
import inputModule from './modules/input'
import clickModule from './modules/click'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    myInput: {
      postion: {
        x: '',
        y: '',
      },
      use: false,
    },
    myShowInput: {
      postion: {
        x: '',
        y: '',
      },
      use: false,
    },
    myTable: {
      postion: {
        x: '',
        y: '',
      },
      use: false,
    },
    myDetail: {
      postion: {
        x: '',
        y: '',
      },
      use: false,
    },
    myCheckDetail: {
      postion: {
        x: '',
        y: '',
      },
      use: false,
    },
  },
  mutations: {
    setMyInput(state, postion) {
      state.myInput = {
        ...state.myInput,
        postion
      }
      postion.x ? state.myInput.use = true : state.myInput.use = false;
    },
    setMyShowInput(state, postion) {
      state.myShowInput = {
        ...state.myShowInput,
        postion
      }
      postion.x ? state.myShowInput.use = true : state.myShowInput.use = false;
    },
    setMyTable(state, postion) {
      state.myTable = {
        ...state.myTable,
        postion
      }
      postion.x ? state.myTable.use = true : state.myTable.use = false;
    },
    setMyDetail(state, postion) {
      state.myDetail = {
        ...state.myDetail,
        postion
      }
      postion.x ? state.myDetail.use = true : state.myDetail.use = false;
    },
    setMyCheckDetail(state, postion) {
      state.myCheckDetail = {
        ...state.myCheckDetail,
        postion
      }
      postion.x ? state.myCheckDetail.use = true : state.myCheckDetail.use = false;
    },
  },
  actions: {},
  modules: {
    inputModule,
    clickModule,
  }
})