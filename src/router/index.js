import Vue from 'vue'
import VueRouter from 'vue-router'
import index from '../views/index.vue'
import vuex_test from '../views/vuex_test.vue'
import show from '../views/show.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    component: index
  },
  {
    path:'/test',
    name:'test',
    component:vuex_test
  },
  {
    path:'/show',
    name:'show',
    component:show
  }
]
const router = new VueRouter({
  routes
})

export default router
