import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import less from 'less'
import ElementUI  from 'element-ui'
import './plugins/element.js'

Vue.use(less)
Vue.use(ElementUI)
Vue.config.productionTip = false

// 定义全局的自定指令（拖拽）
Vue.directive('drag', {
  inserted: function (el) {
    //   获取需要拖拽的元素
    let dragBox = el;
    dragBox.onmousedown = (e) => {
      //计算鼠标和元素边框之间的距离
      let x = e.clientX - dragBox.offsetLeft;
      let y = e.clientY - dragBox.offsetTop;
      document.onmousemove = (e) => {
        // 鼠标现在的位置减去鼠标与元素边框的距离就是元素现在的位置
        let left = e.clientX - x;
        let top = e.clientY - y;
        dragBox.style.left = left + "px";
        dragBox.style.top = top + "px";

        // 改变元素的透明度
        dragBox.style.opacity = 0.5;
      };
      dragBox.onmouseup = (e) => {
        // 抬起鼠标停止移动
        document.onmousemove = null;
        dragBox.style.opacity = 1;
        //防止鼠标移动上去还会移动
        document.onmouseup = null;
      };
    };
  },
})
new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
